<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->helper('form', 'url');
		$this->load->library(array('form_validation', 'session'));
		$this->load->model('login_model');

	}

	public function index() {
		if(isset($this->session->userdata['logged_in']['user'])) {
			redirect('user/add_role');
		}
		else {
			$this->load->view('login');	 
		}
	}

	public function check_login() {

		$this->form_validation->set_rules('user', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean');

		if(sizeof($_POST) > 0) {

			if($this->form_validation->run() == FALSE) {
				if(isset($this->session->userdata['logged_in'])){
					redirect('user/add_role');
				} else {
					$this->load->view('login');
				}
			} 
			else {
				$data = array(
					'user' => $this->input->post('user'),
					'pass' => do_hash($this->input->post('pass'), 'md5')
				);
				$row = $this->login_model->check_login($data);

				if (isset($row)) {
					if($row == true) {
						$user_info = $this->login_model->get_user($data);
						if($user_info != false)
						{
							$user_array = array(
									'user' => $user_info[0]->user_name
								);

							$this->session->set_userdata('logged_in', $user_array);
							redirect('user/add_role');
						}
					}
					else {
						$data['message_display'] = 'Invalid Username or Password';
						$this->load->view('login', $data);
					}
				}
			}

		}

		
	}

	public function logout() {
		$sess_array = array(
			'user' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		$this->load->view('login', $data);
	}


}

?>