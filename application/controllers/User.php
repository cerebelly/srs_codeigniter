<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model('Base_model');
	}

	public function roles()
	{	
		$this->body["form_title"] = "Roles";
		$this->body["roles"] = $this->page_access;
		$this->load->view(HEADER, $this->data);
		$this->load->view('users/role', $this->body);
		$this->load->view(FOOTER);
	}

	public function add_role(){
		$row = $this->Base_model->show_roles($this->current_role);
		$this->data['row'] = json_decode($row[0]->page_access, true);
		// print_r(json_decode($row[0]->page_access, true));
		// exit;
		$this->body["form_title"] = "Add Roles";
		$this->body["roles"] = $this->page_access;
		$this->data['exc'] = $this->page_access['Exclude_functions'];
		$this->load->view(HEADER, $this->data);
		$this->load->view('users/add_role', $this->body);
		$this->load->view(FOOTER);

	}	

	public function get_roles() {
		if(sizeof($_POST) > 0) {
			$roles = array();
			$controllers = $_POST['controller'];
			$user_role = $_POST['user_role'];
			// $user_role = 1;
			foreach ($controllers as $key => $value) $roles[$value] = $_POST[$value];
			// print_r($roles);
			$page_access = json_encode($roles);
			// echo $page_access;
			// exit;
			$this->Base_model->update_roles($user_role, $page_access);

		}

		
	}
	
	
}
