<?php 
class Base_model extends CI_Model {

	public function update_roles($id, $page_access) {
		$data = array(
               'page_access' => $page_access,
        );
		$this->db->where('id', $id);
		$this->db->update('roles', $data);
	}

	public function show_roles($id=null) {
		if($id!=null) $this->db->where("id", $id);
		$query = $this->db->get('roles');
		
		$row = $query->result();
		return $row;
	}

}

?>