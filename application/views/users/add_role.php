 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $form_title; ?>
     </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-3">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <!--  <form role="form"> -->
           <form role="form" method="POST" action="<?php echo base_url('user/get_roles'); ?>">
              <div class="box-body">
                <div class="form-group">
                  <!-- select -->
                  <div class="form-group">
                    <label>Select Role</label>
                    <select class="form-control" name="user_role">
                      <option value="1">Administrator</option>
                      <option value="2">Member</option>
                      <option value="3">Guest</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">

                </div>

              </div>
              <!-- /.box-body -->
            <!-- </form> -->
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->

        <!-- right column -->
        <div class="col-md-9">
          <!-- general form elements -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Classes and Functions</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form" method="POST" action="<?php echo base_url('user/get_roles'); ?>"> -->
              <div class="box-body">
                <table class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Class</th>
                    <th>Functions</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                      
                      $classes = array();
                      $functions = array();
                      #push array value to new array
                      foreach ($row as $class => $function) {
                          array_push($classes, $class);
                          foreach ($function as $key => $value) {
                            array_push($functions, $value);
                          }
                      }
                      // print_r($classes);
                      // print_r($functions);

                      foreach($roles as $key => $role) {
                        if(isset($role["exclude"])) continue;
                         // echo $key; 
                        if($key == 'Exclude_functions') continue;
                  ?>
                    <tr>
                      <td>
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" name="controller[]" class="checkbox_parent" id="<?php echo $key; ?>" value= "<?php echo $key; ?>" <?php echo (in_array($key, $classes)) ? 'checked' : '' ?>> <?php echo $key; ?>
                              </label>
                            </div>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                          <?php 
                           
                            foreach($role["functions"] as $index => $value) {

                          ?>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="<?php echo $key; ?> checkbox_child" name="<?php echo $key; ?>[]" value="<?php echo $index; ?>" <?php echo (in_array($index, $functions)) ? 'checked' : '' ?>> <?php echo $value;?>
                              </label>
                            </div>
                          <?php
                            }
                           ?>
                           

                          </div>

                         
                        </div> 

                      </td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-success pull-right">Save</button>
              </div>

            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>
    $(document).ready(function() {
      $(".checkbox_parent").change(function () {
        var child_class = "."+$(this).attr('id');
        $(child_class).prop('checked', $(this).prop("checked"));
      });

      $(".checkbox_child").on('change', function() {
        var parent_id = "#"+$(this).attr('class').split(' ')[0];
        var child_class = "."+$(this).attr('class').split(' ')[0];
        if ($(child_class).filter(':checked').length > 0) {
          $(parent_id).prop('checked', true);
          console.log($(child_class).filter(':checked').length);
        } else {
          $(parent_id).prop('checked', false);
          console.log($(child_class).filter(':checked').length);
        }
      });
    });
  </script>