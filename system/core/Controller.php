<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	public $roles = array();
	private static $instance;
	public $body = array(), $page_access = array(), $current_role = "" ,$permissions = array(), $links = array(), $base_controller = null, $set_function = null, $data = array();
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;
		//print_r($this->session->userdata('username'));
		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}
		$this->body["form_title"] = "";
		$this->current_role = "1";
		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
		$this->load->helper("global_helper");
		$this->base_controller	 = ucwords($this->router->fetch_class());
		$this->set_function = strtolower($this->router->fetch_method());
		$this->set_access();
		$this->set_roles();
		$this->check_permission();
		$this->get_links();

	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}

	public function set_role(){

	}

	public function load_view($data)
	{
		$this->load->view(HEADER);
		$this->load->view($data);
		$this->load->view(FOOTER);
	}

	public function set_access(){
		$this->page_access = array(
			"Welcome" => array(
				"functions" =>	array(
						"index" =>  'Index',
						"create" => 'Create',
						"view_sample" =>  'View Sample',
						"test" => "Test"
							
				),
				"text" => "Sample Text",
				"icon" => "fa-dashboard"
			),
			"Message" => array(
				"exclude" => true
			),
			"User" => array(
				"functions" =>	array(
						"roles" =>  'Roles',
						"add_role" => 'Add Role',
						"get_roles" => 'Update Roles'

						
				),
				"text" => "Users",
				"icon" => "fa-user"
			)	,
			"Exclude_functions" => array(
				"get_roles",
			)
		);

	}

	
	public function load_error_page(){
		redirect(base_url("message/not_found"));
	}

	public function set_roles(){
		$this->load->model('Base_model');
		$result = $this->Base_model->show_roles($this->current_role);
		if(isset($result)) {
			$data['row'] = $result;
			// print_r($result);
			foreach ($result as $key => $value) {
				$array = json_decode($value->page_access, true);
				$this->permissions[$value->id] = $array;
			
			}
		}

		 /*$this->permissions["1"] = array(
		 	"Welcome" => array("index", "create", "view_sample"),
			"User" => array("roles", "add_role", "get_roles")
		 );*/

	}

	public function check_permission(){
		if(!isset($this->page_access[$this->base_controller]))return true; 
		if(isset($this->page_access[$this->base_controller]["exclude"])) return true;
		if(!$this->permissions[$this->current_role][$this->base_controller]) $this->load_no_access_page();
				if(!in_array($this->set_function ,$this->permissions[$this->current_role][$this->base_controller])) $this->load_no_access_page();
	}

	public function get_links(){
		if(isset($this->permissions[$this->current_role]))
		foreach($this->permissions[$this->current_role] as $page => $functions){
			if(isset($this->page_access[$page])){
				if(!isset($this->page_access[$page]["exclude"])){
					$this->links[$page]["child"] = array();
					$this->links[$page]["icon"] = $this->page_access[$page]["icon"];
					$this->links[$page]["text"] = $this->page_access[$page]["text"];
					foreach($functions as $key =>$function){
						if(isset($this->page_access[$page]["functions"][$function])) {
							$text = $this->page_access[$page]["functions"][$function];
							$url = base_url($page."/".$function);
							$this->links[$page]["child"][$function] = array(
								"text" => $text,
								"url" => $url
							);
						}

					}
				}
			}
		}
		$this->data["front_links"] = $this->links;
		
	}

	public function load_no_access_page(){
		redirect(base_url("message/no_access"));
	}

}
